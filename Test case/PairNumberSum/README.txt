First input is number want to find pair number to sum.
Second input is size of array.
Third input is number inside array.

Example.

7 <--------- want to find number in array can sum to 7
4 <--------- size of array is 4.
1 2 5 6 <------ number in array is {1,2,5,6}