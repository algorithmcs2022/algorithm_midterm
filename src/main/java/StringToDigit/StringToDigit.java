/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StringToDigit;

import java.util.Scanner;

/**
 *
 * @author nasan
 */
public class StringToDigit {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String input = kb.next();
        System.out.println(stringToDigit(input));
    }

    public static int stringToDigit(String input) {
        int sum = 0;

        //Check from last character to first character of input.
        // j variable start from 1, And each loop j will increase by multiply 10 to create place value.
        for (int i = input.length() - 1, j = 1; i >= 0; i--, j *= 10) {

            //Check at position[i] if equal to 0-9 
            if (i == 0 && input.charAt(i) == '-') { //Check if first character is minus sign.
                sum *= -1; //Turn sum to negative value by multiply by -1.
            } else if (input.charAt(i) == '1') {
                sum += 1 * j;   //This digit number will be multiply by j to make value equal to it's place value, then add to sum. 
            } else if (input.charAt(i) == '2') {
                sum += 2 * j;
            } else if (input.charAt(i) == '3') {
                sum += 3 * j;
            } else if (input.charAt(i) == '4') {
                sum += 4 * j;
            } else if (input.charAt(i) == '5') {
                sum += 5 * j;
            } else if (input.charAt(i) == '6') {
                sum += 6 * j;
            } else if (input.charAt(i) == '7') {
                sum += 7 * j;
            } else if (input.charAt(i) == '8') {
                sum += 8 * j;
            } else if (input.charAt(i) == '9') {
                sum += 9 * j;
            }
        }

        return sum;
    }
}
