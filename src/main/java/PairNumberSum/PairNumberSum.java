/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PairNumberSum;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.*;

/**
 *
 * @author nasan
 */
public class PairNumberSum {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        
        //Handle receive input from file
        int c = kb.nextInt();//Number of sum
        int arrIndex = kb.nextInt();//Receive index size.
        int[] A = new int[arrIndex];//Create new array.
        for (int i = 0; i < arrIndex; i++) {//Loop for receive input from file.
            A[i] = kb.nextInt();
        }

        System.out.println(findPair(A, c));

    }

    public static List<String> findPair(int[] A, int c) {

        List<String> result = new ArrayList<String>(); //Create empty list of string to contain multiple scenario result.

        //Add every number in array A to set to delete duplicate number.
        Set<Integer> set = new HashSet<Integer>();//Create empty set.
        for (int i = 0; i < A.length; i++) {
            set.add(A[i]);//Add number at position i to set.
        }

        //Convert set back to array
        int[] nonDuplicateArray = new int[set.size()];//Create new array, length of index equal to size of set.
        int index = 0;//Start index;
        for (int x : set) {//For each element in set.
            nonDuplicateArray[index++] = x;//Add number to array. 
        }

        //Check at every number if sum to c.
        for (int i = 0; i < nonDuplicateArray.length - 1; i++) {//Start from first index to second last index;
            for (int j = i + 1; j < nonDuplicateArray.length; j++) {//Start from second index to last index;
                if (nonDuplicateArray[i] + nonDuplicateArray[j] == c) {
                    result.add(c + " = " + nonDuplicateArray[i] + "+" + nonDuplicateArray[j] + " ");//Add to result
                }
            }
        }
        //Check if result is empty return no pair possible.
        if (result.isEmpty()) {
            result.add("No pair number can sum to " + c);
        }

        return result;
    }
}
