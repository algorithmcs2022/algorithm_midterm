Input is number in string type.
Output is number in int type.

Example.
input(String) = 00578
output(Int) = 578